public class Dog extends Animal{
    public boolean canFly(){
        return true;
    }

    @Override
    public void eat(){
        System.out.println("I eat dog food!");
    }
}
